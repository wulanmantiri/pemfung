-- Buatlah fungsi ​myCurry​ dan myUncurry​ yang memiliki
-- sifat seperti fungsi curry dan uncurry pada library Haskell
myCurry :: ((a, b) -> c) -> a -> b -> c
myCurry f a b = f (a, b)

myUncurry :: (a -> b -> c) -> (a, b) -> c
myUncurry f (a, b) = f a b
--

-- Buatlah fungsi fibonacci yang menerima bilangan bulat
-- positif n dan mengembalikan list yang
-- berisi bilangan fibonacci dari 0 sampai n
sumFib :: Int -> Int -> Int -> [Int]
sumFib a b num
    | a + b > num = []
    | otherwise = (a + b):sumFib b (a + b) num

fibonacci :: Int -> [Int]
fibonacci num = 0:1:sumFib 0 1 num
--

-- Buatlah fungsi ​ power​ namun hanya dengan menggunakan
-- operasi penjumlahan (+)
power :: Int -> Int -> Int
power 0 _ = 0
power _ 0 = 1
power num pow = foldl (\x y -> num + x) 0 [1..power num (pow-1)]
--

-- Buatlah fungsi ​sumEven yang menerima list dari bilangan
-- asli dan menjumlahkan bilangan-bilangan genap saja.
sumEven :: [Int] -> Int
sumEven = foldl (+) 0 . filter even
--

-- Buatlah sebuah kalkulator investasi sederhana berupa fungsi ​
-- invest​ yang menerima 3 buah parameter, yaitu:
-- - nominal tiap bulan
-- - return investasi tiap bulan (%)
-- - durasi (bulan)
addInterest :: Double -> Double -> Double
addInterest rate nominal = nominal * (1 + rate/100)

invest :: Double -> Double -> Int -> Double
invest nominal rate month = addInterest rate $ 
    foldl (\x y -> addInterest rate x + nominal) 0 [1..month]
--

main :: IO()
main = do
    putStr $ "1. myUncurry (+) (2,3) = "
    print $ myUncurry (+) (2,3)
    putStr $ "2. fibonacci 4 = "
    print $ fibonacci 4
    putStr "3. power 3 2 = "
    print $ power 3 2
    putStr $ "4. sumEven [1..6] = "
    print $ sumEven [1..6]
    putStr $ "5. invest 100000 0.3 2 = "
    print $ invest 100000 0.3 2
