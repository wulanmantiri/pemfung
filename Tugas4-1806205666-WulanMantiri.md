1. Buatlah definisi fungsi `length` baru menggunakan map dan fold!
```
length' :: [a] -> Int
length' = foldl (+) 0 . map (\x -> 1)
```

Saya membuat fungsi `length` dengan fungsi `map` untuk menggantikan semua elemen dalam list input dengan angka 1. Angka 1 ini nanti akan dijumlahkan untuk menghitung jumlah elemen dengan `fold`.

2. Diberikan fungsi `addUp ns = filter greaterOne (map addOne ns)` dimana `greaterOne n = n > 1` dan `addOne n = n + 1`, definisikan ulang fungsi tersebut dengan filter sebelum map, misalnya addUp ns = map fun1 (filter fun2 ns)!
```
addUp :: [Int] -> [Int]
addUp = map addOne . filter (greaterOne . addOne)
```

Saya membuat fungsi `addUp` dengan fungsi `greaterOne` dan `addOne` yang sama dengan soal. Saya mengecek jika elemen n memenuhi n + 1 > 1 dengan `greaterOne . addOne`. Setelah memuat hanya elemen yang memenuhi dengan `filter`, saya mengubah semua elemen n menjadi n + 1 dengan `map`.

3. Definisikan fungsi sum of the squares dari 1 sampai n dengan cara berikut!

    * a. map dan fold
    ```
    sumOfSquares :: Int -> Int
    sumOfSquares n = foldl (+) 0 $ map (^2) [1..n]
    ```

    Pertama, saya *generate* list dari 1 sampai angka input n dengan list comprehension. Saya lalu pakai fungsi `map` untuk mengubah setiap elemen n menjadi pangkatnya (n^2) untuk nanti dijumlahkan dengan fungsi `fold`.

    * b. fold and list comprehension
    ```
    sumOfSquares' :: Int -> Int
    sumOfSquares' n = foldl (\x y -> x + y^2) 0 [1..n]
    ```

    Pertama, saya *generate* list dari 1 sampai angka input n dengan list comprehension. Saya lalu pakai fungsi `fold` untuk menjumlahkan akumulasi dari pangkat setiap elemen di list tersebut.
    
    * c. Jelaskan perbedaan dua pendekatan tersebut!

    Kedua fungsi yang saya buat menggunakan *list comprehension* untuk *generate* list dari 1 hingga n. Perbedaannya adalah `map` harus membuat sebuah list baru berisi hasil dari pangkat semua elemen untuk dijumlahkan, sedangkan jika menggunakan `fold` saja, elemen akan langsung diiterasi untuk dihitung pangkatnya dan dijumlah dengan hasil akumulasi.
    

4. Definisikan fungsi yang mengembalikan jumlah bilangan kelipatan 5 dalam sebuah list!
```
multipleOf5 :: [Int] -> Int
multipleOf5 = length . filter ((==) 0 . mod 5)
```

Saya membuat fungsi `multipleOf5` dengan `filter` list untuk yang berkelipatan 5 (atau yang mod 5 == 0) saja, lalu saya menggunakan fungsi *built-in* `length` untuk menghitung jumlah elemen dalam list tersebut.

5. Definisikan fungsi total dimana `total :: (Int -> Int) -> (Int -> Int)` sehingga total f adalah fungsi ketika mendapat nilai n memberikan total dari f 0 + f 1 + … + f n!
```
total :: (Int -> Int) -> (Int -> Int)
total f n = sum $ map f [0..n]
```

Saya membuat fungsi `total` dengan `map` untuk membuat list dari 0 hingga `angka input n` yang diaplikasikan `fungsi f dari input`, lalu saya menggunakan fungsi *built-in* `sum` untuk menghitung jumlah semua elemen di list tersebut.

6. Buatlah fungsi `reverse` dengan menggunakan foldr!
```
reverse' :: [a] -> [a]
reverse' = foldr (\x y -> y ++ [x]) []
```

Saya membuat fungsi `reverse` dengan `++` untuk menyesuaikan dengan penerapan `foldr` dimana iterasi dimulai dari starting value (`[]`) *concat* dengan `[elemenPertama]`. Sintaks `:` lebih cocok untuk `foldl` dimana base atau end valuenya adalah `[]`.

7. Uraikan langkah evaluasi dari ekspresi berikut: `[x+y|x<-[1..4], y<-[2..4], x > y]` !

Mulai dari list kosong, misal `lst = []`.

Iterasi akan berjalan layaknya sebuah `for` loop. Untuk kasus ini, `for x in [1,2,3,4]` memanggil `for y in [2,3,4]` didalamnya.

* `x = 1, y = 2` -> `x > y` ? False
* `x = 1, y = 3` -> `x > y` ? False
* `x = 1, y = 4` -> `x > y` ? False
* `x = 2, y = 2` -> `x > y` ? False
* `x = 2, y = 3` -> `x > y` ? False
* `x = 2, y = 4` -> `x > y` ? False
* `x = 3, y = 2` -> `x > y` ? True, maka ditambahkan ke lst jadi `lst = [(3+2)]`
* `x = 3, y = 3` -> `x > y` ? False
* `x = 3, y = 4` -> `x > y` ? False
* `x = 4, y = 2` -> `x > y` ? True, maka ditambahkan ke lst jadi `lst = [5, (4+2)]`
* `x = 4, y = 3` -> `x > y` ? True, maka ditambahkan ke lst jadi `lst = [5, 6, (4+3)]`
* `x = 4, y = 4` -> `x > y` ? False

Iterasi berhenti dan list comprehension mengembalikan isi lst yaitu `[5,6,7]`.

8. Buatlah definisi infinite list dari triple pythagoras. List tersebut terdiri dari elemen triple bilangan bulat positif yang mengikut persamaan pythagoras x2 + y2 = z2 !
```
pythaTriple :: [(Int, Int, Int)]
pythaTriple = [(x,y,z) | z <- [5..], y <- [(z-1),(z-2)..1], x <- [(y-1),(y-2)..1], x^2 + y^2 == z^2]
```

Saya membuat fungsi `pythaTriple` dengan *list comprehension* dengan iterasi list angka potensial `z` terlebih dahulu dengan angka awal 5, diikuti dengan `y` lalu `x`. List angka `y` dan `x` merupakan list terbalik dari angka `z` (untuk `y`) dan `y` (untuk `x`) menurun sampai 1. Pasangan `x,y,z` nanti dicek jika memenuhi persamaan `x2 + y2 = z2`. Jika memenuhi, pasangan `(x,y,z)` akan dimasukkan dalam list.

9. Sieve of Eratosthenes merupakan algoritma yang digunakan untuk mengeliminasi angka yang bukan bilangan prima dari kumpulan angka yang ada. Definisi dalam Haskell adalah sebagai berikut:
```
primes = sieve [2..]
sieve (p:xs) = p : sieve [x | x <− xs, x `mod` p > 0]
```
Jelaskan dalam minimal 3 kalimat proses evaluasi dari fungsi sieve tersebut!

Fungsi `sieve` merupakan fungsi rekursi list dimana elemen pertama (`p`) akan ditambahkan ke list. Untuk list yang menampung elemen-elemen selain yang pertama (`xs`), *list comprehension* membuat list baru yang hanya menampung value yang bukan kelipatan `p`. *List comprehension* tersebut dapat ditulis dengan fungsi `filter` yang juga bersifat lazy seperti:
``sieve' (p:xs) = p : sieve (filter (\x -> x `mod` p > 0) xs)``

Untuk fungsi `primes`, fungsi `sieve` dimulai dari angka prima pertama yaitu 2. Langkah evaluasi proses `sieve` dalam `primes` adalah sebagai berikut:

* `2: sieve [3,5,7,9,11,..]` -> list tidak memasukkan kelipatan 2
* `2: 3: sieve [5,7,11,13,17,..]` -> mengambilkan list sebelumnya tapi tidak memasukkan kelipatan 3
* `2: 3: 5: sieve [7,11,13,17,19..]` -> mengambilkan list sebelumnya lagi tapi tidak memasukkan kelipatan 5
* dan seterusnya

10. Buatlah fungsi `noUpperAndIdent` yang menghapus seluruh karakter kapital  dan karakter non-alfabet dari argumen String yang diberikan! (Hint: Gunakan library function elem dan isUpper)
```
noUpperAndIdent :: String -> String
noUpperAndIdent = filter (`elem` ['a'..'z'])
```

Saya membuat fungsi `noUpperAndIdent` dengan `elem` untuk mengecek jika suatu elemen merupakan sebuah huruf kecil alfabet (a-z) yang sama dengan fungsi `isLower`. Saya pun menerapkan `filter` pada elemen yang tidak memenuhi syarat tersebut.
