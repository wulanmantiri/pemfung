circleArea :: Double -> Double
circleArea r = pi * r^2

isTriangle :: Int -> Int -> Int -> Bool
isTriangle a b c = a + b > c && a + c > b && b + c > a

listSum :: [Int] -> Int
listSum (num: rest) = num + listSum rest
listSum [] = 0

listSumArea :: [Double] -> Double
listSumArea (area: rest) = area + listSumArea rest
listSumArea [] = 0

reverseList :: [any] -> [any]
reverseList [] = []
reverseList lst = last lst : reverseList (init lst)

quicksort1 :: (Ord any) => [any] -> [any]
quicksort1 [] = []
quicksort1 (a: rest) =
    let smallerSorted = quicksort1 (filter (<=a) rest)
        biggerSorted = quicksort1 (filter (>a) rest)
    in  biggerSorted ++ [a] ++ smallerSorted

main :: IO()
main = do
    putStr "1. circleArea 2 = "
    print $ circleArea 2
    putStr "2. isTriangle 2 3 4 = "
    print $ isTriangle 2 3 4
    putStr "3. listSum [2,3,4] = "
    print $ listSum [2,3,4]
    putStr "4. listSumArea [circleArea 2, circleArea 3, circleArea 4] = "
    print $ listSumArea [circleArea 2, circleArea 3, circleArea 4]
    putStr "5. reverseList [1,2,3,4] = "
    print $ reverseList [1,2,3,4]
    putStr "6. quicksort1 [4,1,5,6,1,3] = "
    print $ quicksort1 [4,1,5,6,1,3]
