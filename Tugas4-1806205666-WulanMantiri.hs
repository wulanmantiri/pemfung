-- 1. Buatlah definisi fungsi length baru menggunakan map dan fold!
length' :: [a] -> Int
length' = foldl (+) 0 . map (\x -> 1)
--

-- 2. Diberikan fungsi addUp ns = filter greaterOne (map addOne ns) dimana
-- greaterOne n = n > 1 dan addOne n = n + 1, definisikan ulang fungsi tersebut 
-- dengan filter sebelum map, misalnya addUp ns = map fun1 (filter fun2 ns)!
greaterOne :: Int -> Bool
greaterOne n = n > 1

addOne :: Int -> Int
addOne n = n + 1

addUp :: [Int] -> [Int]
addUp = map addOne . filter (greaterOne . addOne)
--

-- 3. Definisikan fungsi sum of the squares dari 1 sampai n dengan cara berikut!
sumOfSquares, sumOfSquares' :: Int -> Int

-- a. map dan fold
sumOfSquares n = foldl (+) 0 $ map (^2) [1..n]

-- b. fold and list comprehension
sumOfSquares' n = foldl (\x y -> x + y^2) 0 [1..n]
--

-- 4. Definisikan fungsi yang mengembalikan jumlah bilangan 
-- kelipatan 5 dalam sebuah list!
multipleOf5 :: [Int] -> Int
multipleOf5 = length . filter ((==) 0 . mod 5)
--

-- 5. Definisikan fungsi total dimana total :: (Int -> Int) -> (Int -> Int) sehingga
-- total f adalah fungsi ketika mendapat nilai n memberikan total dari f 0 + f 1 + … + f n!
total :: (Int -> Int) -> (Int -> Int)
total f n = sum $ map f [0..n]
--

-- 6. Buatlah fungsi reverse dengan menggunakan foldr!
reverse' :: [a] -> [a]
reverse' = foldr (\x y -> y ++ [x]) []
--

-- 8. Buatlah definisi infinite list dari triple pythagoras. List tersebut terdiri dari elemen triple
-- bilangan bulat positif yang mengikut persamaan pythagoras x2 + y2 = z2 !
pythaTriple :: [(Int, Int, Int)]
pythaTriple = [(x,y,z) | z <- [5..], y <- [(z-1),(z-2)..1], x <- [(y-1),(y-2)..1], x^2 + y^2 == z^2]
--

-- 10. Buatlah fungsi noUpperAndIdent yang menghapus seluruh karakter kapital 
-- dan karakter non-alfabet dari argumen String yang diberikan!
-- (Hint: Gunakan library function elem dan isUpper)
noUpperAndIdent :: String -> String
noUpperAndIdent = filter (`elem` ['a'..'z'])
--

main :: IO()
main = do
    putStr "2. addUp [0,1,2,3] = "
    print $ addUp [0,1,2,3]
    putStr "3a. (with map and fold) sumOfSquares 3 = "
    print $ sumOfSquares 3
    putStr "3b. (with fold and list comprehension) sumOfSquares 3 = "
    print $ sumOfSquares' 3
    putStr "4. multipleOf5 [1,2,3,4,5,6,7,8,9,10] = "
    print $ multipleOf5 [1,2,3,4,5,6,7,8,9,10]
    putStr "5. total (+1) 3 = "
    print $ total (+1) 3
    putStr "6. reverse [1,2,3,4,5] = "
    print $ reverse' [1,2,3,4,5]
    putStr "8. take 6 pythaTriple = "
    print $ take 6 pythaTriple
    putStr "10. noUpperAndIdent \"FunPro MOOC\" = "
    print $ noUpperAndIdent "FunPro MOOC"
