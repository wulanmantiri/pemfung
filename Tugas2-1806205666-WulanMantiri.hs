import Data.Char

sumOfSquares :: [Int] -> Int
sumOfSquares (num: nums) = num^2 + sumOfSquares nums
sumOfSquares [] = 0

triangular :: Int -> Int
triangular num
    | num <= 0 = 0
    | otherwise = num + triangular (num-1)

power :: Int -> Int -> Int
power num 1 = num
power num pow = num * power num (pow-1)

-- helper functions for number 4
notPunct :: Char -> Bool
notPunct char = isAlpha char || isDigit char

palindrome :: String -> Bool
palindrome str = str == (reverse str)
--

isPalindrome :: String -> Bool
isPalindrome str = palindrome $ map toLower $ filter notPunct str

main :: IO()
main = do
    putStr "1. sumOfSquares [1,2,3] = "
    print $ sumOfSquares [1,2,3]
    putStr "2. triangular 5 = "
    print $ triangular 5
    putStr "3. power 3 2 = "
    print $ power 3 2
    putStr "4. isPalindrome \"Madam, I'm Adam\" = "
    print $ isPalindrome "Madam, I'm Adam"
